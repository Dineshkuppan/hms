## BITS Pilani - Hospital Management System


### Compile the package
```
mvn clean package -DskipTests

```
### Run Spring Boot application
```
mvn spring-boot:run
```

### Open API & Swagger UI
```
http://localhost:5800/swagger-ui/index.html
```

####API List

![open-api.png](open-api.png)

####Swagger UI
![swagger-ui.png](swagger-ui.png)