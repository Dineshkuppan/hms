package com.bits.pilani.hms.spring.datajpa.repository;

import com.bits.pilani.hms.spring.datajpa.model.LabTests;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LabTestsRepository extends JpaRepository<LabTests, Long> {

}
