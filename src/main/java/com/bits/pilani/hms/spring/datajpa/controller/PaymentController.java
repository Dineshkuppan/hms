package com.bits.pilani.hms.spring.datajpa.controller;

import com.bits.pilani.hms.spring.datajpa.model.Payment;
import com.bits.pilani.hms.spring.datajpa.repository.PaymentRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class PaymentController {
  @Autowired
  PaymentRepository paymentRepository;

  @GetMapping("/payment/all")
  public ResponseEntity<List<Payment>> getAllPayments(
      @RequestParam(required = false) String paymentId) {
    try {
      List<Payment> paymentList = new ArrayList<>();

      if (paymentId == null) {
        paymentList.addAll(paymentRepository.findAll());
      }

      if (paymentList.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(paymentList, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/payment/{id}")
  public ResponseEntity<Payment> getPaymentById(@PathVariable("id") long id) {
    Optional<Payment> recordByData = paymentRepository.findById(id);
    return recordByData.map(record -> new ResponseEntity<>(record, HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PostMapping("/payment")
  public ResponseEntity<Payment> addPayment(@RequestBody Payment Payment) {
    paymentRepository.save(Payment);
    try {
      return new ResponseEntity<>(Payment, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/payment/{id}")
  public ResponseEntity<Payment> updatePayment(@PathVariable("id") long id,
      @RequestBody Payment Payment) {
    Optional<Payment> PaymentData = paymentRepository.findById(id);

    if (PaymentData.isPresent()) {
      return new ResponseEntity<>(paymentRepository.save(Payment), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/payment/{id}")
  public ResponseEntity<HttpStatus> deletePaymentById(@PathVariable("id") long id) {
    try {
      paymentRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
