package com.bits.pilani.hms.spring.datajpa.controller;

import com.bits.pilani.hms.spring.datajpa.model.BillSummary;
import com.bits.pilani.hms.spring.datajpa.repository.BillSummaryRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class BillSummaryController {

  @Autowired
  BillSummaryRepository billSummaryRepository;

  @GetMapping("/billsummary/all")
  public ResponseEntity<List<BillSummary>> getAllBillSummaries(
      @RequestParam(required = false) String summaryId) {
    try {
      List<BillSummary> summaries = new ArrayList<>();

      if (summaryId == null) {
        summaries.addAll(billSummaryRepository.findAll());
      }

      if (summaries.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(summaries, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/billsummary/{id}")
  public ResponseEntity<BillSummary> getBillSummaryById(@PathVariable("id") long id) {
    Optional<BillSummary> tutorialData = billSummaryRepository.findById(id);
    return tutorialData.map(summary -> new ResponseEntity<>(summary, HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PostMapping("/billsummary")
  public ResponseEntity<BillSummary> addBillSummary(@RequestBody BillSummary billSummary) {
    billSummaryRepository.save(billSummary);
    try {
      return new ResponseEntity<>(billSummary, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/billsummary/{id}")
  public ResponseEntity<BillSummary> updateBillSummary(@PathVariable("id") long id,
      @RequestBody BillSummary billSummary) {
    Optional<BillSummary> billSummaryData = billSummaryRepository.findById(id);

    if (billSummaryData.isPresent()) {
      return new ResponseEntity<>(billSummaryRepository.save(billSummary), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/billsummary/{id}")
  public ResponseEntity<HttpStatus> deleteBillSummaryById(@PathVariable("id") long id) {
    try {
      billSummaryRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
