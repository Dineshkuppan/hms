package com.bits.pilani.hms.spring.datajpa.repository;

import com.bits.pilani.hms.spring.datajpa.model.MedicalHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicalHistoryRepository extends JpaRepository<MedicalHistory, Long> {

}
