package com.bits.pilani.hms.spring.datajpa.model;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "medical_history")
@Setter
@Getter
@ToString
public class MedicalHistory {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Column(name = "disease_name")
  private String disease_name;

  @Column(name = "diagnosed_date")
  private String diagnosed_date;

  @Column(name = "description")
  private String description;

  @Column(nullable = false, updatable = false, name = "created_date")
  @CreationTimestamp
  private LocalDateTime created_date;

  @Column(nullable = false, updatable = false, name = "last_updated_date")
  @CreationTimestamp
  private LocalDateTime last_updated_date;

  @Column(name = "created_by")
  private String created_by;

  @Column(name = "last_updated_by")
  private String last_updated_by;

  public MedicalHistory() {

  }

}
