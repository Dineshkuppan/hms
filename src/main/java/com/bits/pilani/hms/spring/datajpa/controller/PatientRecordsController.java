package com.bits.pilani.hms.spring.datajpa.controller;

import com.bits.pilani.hms.spring.datajpa.model.PatientRecords;
import com.bits.pilani.hms.spring.datajpa.repository.PatientRecordsRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class PatientRecordsController {

  @Autowired
  PatientRecordsRepository patientRecordsRepository;

  @GetMapping("/patient-records/all")
  public ResponseEntity<List<PatientRecords>> getAllPatientRecords(
      @RequestParam(required = false) String patientRecordsId) {
    try {
      List<PatientRecords> patientRecords = new ArrayList<>();

      if (patientRecordsId == null) {
        patientRecords.addAll(patientRecordsRepository.findAll());
      }

      if (patientRecords.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(patientRecords, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/patient-records/{id}")
  public ResponseEntity<PatientRecords> getPatientRecordsById(@PathVariable("id") long id) {
    Optional<PatientRecords> recordByData = patientRecordsRepository.findById(id);
    return recordByData.map(record -> new ResponseEntity<>(record, HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PostMapping("/patient-records")
  public ResponseEntity<PatientRecords> addPatientRecords(
      @RequestBody PatientRecords PatientRecords) {
    patientRecordsRepository.save(PatientRecords);
    try {
      return new ResponseEntity<>(PatientRecords, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/patient-records/{id}")
  public ResponseEntity<PatientRecords> updatePatientRecords(@PathVariable("id") long id,
      @RequestBody PatientRecords PatientRecords) {
    Optional<PatientRecords> PatientRecordsData = patientRecordsRepository.findById(id);

    if (PatientRecordsData.isPresent()) {
      return new ResponseEntity<>(patientRecordsRepository.save(PatientRecords), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/patient-records/{id}")
  public ResponseEntity<HttpStatus> deletePatientRecordsById(@PathVariable("id") long id) {
    try {
      patientRecordsRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
