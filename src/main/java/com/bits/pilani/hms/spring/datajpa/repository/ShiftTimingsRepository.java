package com.bits.pilani.hms.spring.datajpa.repository;

import com.bits.pilani.hms.spring.datajpa.model.ShiftTimings;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShiftTimingsRepository extends JpaRepository<ShiftTimings, Long> {

}
