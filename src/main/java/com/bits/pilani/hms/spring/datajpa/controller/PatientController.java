package com.bits.pilani.hms.spring.datajpa.controller;

import com.bits.pilani.hms.spring.datajpa.model.Patient;
import com.bits.pilani.hms.spring.datajpa.repository.PatientRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class PatientController {

  @Autowired
  PatientRepository patientRepository;

  @GetMapping("/patient/all")
  public ResponseEntity<List<Patient>> getAllPatients(
      @RequestParam(required = false) String patientId) {
    try {
      List<Patient> patientList = new ArrayList<>();

      if (patientId == null) {
        patientList.addAll(patientRepository.findAll());
      }

      if (patientList.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(patientList, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/patient/{id}")
  public ResponseEntity<Patient> getPatientById(@PathVariable("id") long id) {
    Optional<Patient> tutorialData = patientRepository.findById(id);
    return tutorialData.map(patient -> new ResponseEntity<>(patient, HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PostMapping("/patient")
  public ResponseEntity<Patient> addPatient(@RequestBody Patient patient) {
    patientRepository.save(patient);
    try {
      return new ResponseEntity<>(patient, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/patient/{id}")
  public ResponseEntity<Patient> updatePatientInfo(@PathVariable("id") long id,
      @RequestBody Patient patient) {
    Optional<Patient> tutorialData = patientRepository.findById(id);

    if (tutorialData.isPresent()) {
      Patient patient1 = tutorialData.get();
      patient1.setFirst_name(patient.getFirst_name());
      patient1.setLast_name(patient.getLast_name());
      patient1.setPatient_age(patient.getPatient_age());
      patient1.setGender(patient.getGender());
      patient1.setAddress_line_1(patient.getAddress_line_1());
      patient1.setAddress_line_2(patient.getAddress_line_2());
      return new ResponseEntity<>(patientRepository.save(patient1), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/patient/{id}")
  public ResponseEntity<HttpStatus> deletePatientById(@PathVariable("id") long id) {
    try {
      patientRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
