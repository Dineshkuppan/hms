package com.bits.pilani.hms.spring.datajpa.controller;

import com.bits.pilani.hms.spring.datajpa.model.Visitors;
import com.bits.pilani.hms.spring.datajpa.repository.VisitorRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class VisitorController {

  @Autowired
  VisitorRepository visitorRepository;

  @GetMapping("/visitors/all")
  public ResponseEntity<List<Visitors>> getAllVisitors(
      @RequestParam(required = false) String visitorId) {
    try {
      List<Visitors> Visitors = new ArrayList<>();

      if (visitorId == null) {
        Visitors.addAll(visitorRepository.findAll());
      }

      if (Visitors.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(Visitors, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/visitors/{id}")
  public ResponseEntity<Visitors> getVisitorById(@PathVariable("id") long id) {
    Optional<Visitors> recordByData = visitorRepository.findById(id);
    return recordByData.map(record -> new ResponseEntity<>(record, HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PostMapping("/visitors")
  public ResponseEntity<Visitors> addVisitor(@RequestBody Visitors visitors) {
    visitorRepository.save(visitors);
    try {
      return new ResponseEntity<>(visitors, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/visitors/{id}")
  public ResponseEntity<Visitors> updateVisitor(@PathVariable("id") long id,
      @RequestBody Visitors Visitor) {
    Optional<Visitors> optionalVisitors = visitorRepository.findById(id);

    if (optionalVisitors.isPresent()) {
      return new ResponseEntity<>(visitorRepository.save(Visitor), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/visitors/{id}")
  public ResponseEntity<HttpStatus> deleteVisitorById(@PathVariable("id") long id) {
    try {
      visitorRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
