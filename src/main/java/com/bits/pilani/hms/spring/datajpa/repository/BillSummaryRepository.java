package com.bits.pilani.hms.spring.datajpa.repository;

import com.bits.pilani.hms.spring.datajpa.model.BillSummary;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BillSummaryRepository extends JpaRepository<BillSummary, Long> {

}
