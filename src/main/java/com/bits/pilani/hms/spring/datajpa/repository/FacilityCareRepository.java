package com.bits.pilani.hms.spring.datajpa.repository;

import com.bits.pilani.hms.spring.datajpa.model.FacilityCare;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FacilityCareRepository extends JpaRepository<FacilityCare, Long> {

}
