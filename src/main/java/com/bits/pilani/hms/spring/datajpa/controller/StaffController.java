package com.bits.pilani.hms.spring.datajpa.controller;

import com.bits.pilani.hms.spring.datajpa.model.HospitalStaff;
import com.bits.pilani.hms.spring.datajpa.repository.StaffRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class StaffController {

  @Autowired
  StaffRepository staffRepository;

  @GetMapping("/staff/all")
  public ResponseEntity<List<HospitalStaff>> getAllStaffs(
      @RequestParam(required = false) String staffId) {
    try {
      List<HospitalStaff> hospitalStaffList = new ArrayList<>();

      if (staffId == null) {
        hospitalStaffList.addAll(staffRepository.findAll());
      }

      if (hospitalStaffList.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(hospitalStaffList, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/staff/{id}")
  public ResponseEntity<HospitalStaff> getStaffById(@PathVariable("id") long id) {
    Optional<HospitalStaff> recordByData = staffRepository.findById(id);
    return recordByData.map(record -> new ResponseEntity<>(record, HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PostMapping("/staff")
  public ResponseEntity<HospitalStaff> addStaff(@RequestBody HospitalStaff Staff) {
    staffRepository.save(Staff);
    try {
      return new ResponseEntity<>(Staff, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/staff/{id}")
  public ResponseEntity<HospitalStaff> updateStaff(@PathVariable("id") long id,
      @RequestBody HospitalStaff hospitalStaff) {
    Optional<HospitalStaff> optionalHospitalStaff = staffRepository.findById(id);

    if (optionalHospitalStaff.isPresent()) {
      return new ResponseEntity<>(staffRepository.save(hospitalStaff), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/staff/{id}")
  public ResponseEntity<HttpStatus> deleteStaffById(@PathVariable("id") long id) {
    try {
      staffRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
