package com.bits.pilani.hms.spring.datajpa.model;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "drug")
@Setter
@Getter
@ToString
public class Drug {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Column(name = "drug_name")
  private String drug_name;

  @Column(name = "manufacturer_name")
  private String manufacturer_name;

  @Column(name = "description")
  private String description;

  @Column(name = "dosage_details")
  private String dosage_details;

  @Column(name = "available")
  private String available;

  @Column(name = "manufacturer_date")
  private LocalDateTime manufacturer_date;

  @Column(name = "expiry_date")
  private String expiry_date;

  @Column(name = "validity")
  private String validity;

  @Column(nullable = false, updatable = false, name = "created_date")
  @CreationTimestamp
  private LocalDateTime created_date;

  @Column(nullable = false, updatable = false, name = "last_updated_date")
  @CreationTimestamp
  private LocalDateTime last_updated_date;

  @Column(name = "created_by")
  private String created_by;

  @Column(name = "last_updated_by")
  private String last_updated_by;

  public Drug() {
  }
}
