package com.bits.pilani.hms.spring.datajpa.repository;

import com.bits.pilani.hms.spring.datajpa.model.Physician;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhysicianRepository extends JpaRepository<Physician, Long> {

}
