package com.bits.pilani.hms.spring.datajpa.model;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "patient_records")
@Setter
@Getter
@ToString
public class PatientRecords {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Column(name = "medical_history_id")
  private String medical_history_id;

  @Column(name = "drug_id")
  private String drug_id;

  @Column(name = "patient_test_results")
  private String patient_test_results;

  @Column(name = "treatment")
  private String treatment;

  @Column(name = "patient_id")
  private String patient_id;

  @Column(name = "date_admitted")
  private String date_admitted;

  @Column(name = "date_discharged")
  private String date_discharged;

  @Column(nullable = false, updatable = false, name = "created_date")
  @CreationTimestamp
  private LocalDateTime created_date;

  @Column(nullable = false, updatable = false, name = "last_updated_date")
  @CreationTimestamp
  private LocalDateTime last_updated_date;

  @Column(name = "created_by")
  private String created_by;

  @Column(name = "last_updated_by")
  private String last_updated_by;

  public PatientRecords() {

  }

}
