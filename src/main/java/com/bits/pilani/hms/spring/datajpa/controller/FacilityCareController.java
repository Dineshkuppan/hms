package com.bits.pilani.hms.spring.datajpa.controller;

import com.bits.pilani.hms.spring.datajpa.model.FacilityCare;
import com.bits.pilani.hms.spring.datajpa.repository.FacilityCareRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class FacilityCareController {

  @Autowired
  FacilityCareRepository facilityCareRepository;

  @GetMapping("/facilitycare/all")
  public ResponseEntity<List<FacilityCare>> getAllFacilityCares(
      @RequestParam(required = false) String facilityCareId) {
    try {
      List<FacilityCare> facilityCares = new ArrayList<>();

      if (facilityCareId == null) {
        facilityCares.addAll(facilityCareRepository.findAll());
      }

      if (facilityCares.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(facilityCares, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/facilitycare/{id}")
  public ResponseEntity<FacilityCare> getFacilityCareById(@PathVariable("id") long id) {
    Optional<FacilityCare> recordByData = facilityCareRepository.findById(id);
    return recordByData.map(record -> new ResponseEntity<>(record, HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PostMapping("/facilitycare")
  public ResponseEntity<FacilityCare> addFacilityCare(@RequestBody FacilityCare FacilityCare) {
    facilityCareRepository.save(FacilityCare);
    try {
      return new ResponseEntity<>(FacilityCare, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/facilitycare/{id}")
  public ResponseEntity<FacilityCare> updateFacilityCare(@PathVariable("id") long id,
      @RequestBody FacilityCare FacilityCare) {
    Optional<FacilityCare> FacilityCareData = facilityCareRepository.findById(id);

    if (FacilityCareData.isPresent()) {
      return new ResponseEntity<>(facilityCareRepository.save(FacilityCare), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/facilitycare/{id}")
  public ResponseEntity<HttpStatus> deleteFacilityCareById(@PathVariable("id") long id) {
    try {
      facilityCareRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
