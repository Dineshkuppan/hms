package com.bits.pilani.hms.spring.datajpa.controller;

import com.bits.pilani.hms.spring.datajpa.model.MedicalHistory;
import com.bits.pilani.hms.spring.datajpa.repository.MedicalHistoryRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class MedicalHistoryController {

  @Autowired
  MedicalHistoryRepository medicalHistoryRepository;

  @GetMapping("/medical-history/all")
  public ResponseEntity<List<MedicalHistory>> getAllMedicalHistorys(
      @RequestParam(required = false) String medicalHistoryId) {
    try {
      List<MedicalHistory> medicalHistories = new ArrayList<>();

      if (medicalHistoryId == null) {
        medicalHistories.addAll(medicalHistoryRepository.findAll());
      }

      if (medicalHistories.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(medicalHistories, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/medical-history/{id}")
  public ResponseEntity<MedicalHistory> getMedicalHistoryById(@PathVariable("id") long id) {
    Optional<MedicalHistory> recordByData = medicalHistoryRepository.findById(id);
    return recordByData.map(record -> new ResponseEntity<>(record, HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PostMapping("/medical-history")
  public ResponseEntity<MedicalHistory> addMedicalHistory(
      @RequestBody MedicalHistory MedicalHistory) {
    medicalHistoryRepository.save(MedicalHistory);
    try {
      return new ResponseEntity<>(MedicalHistory, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/medical-history/{id}")
  public ResponseEntity<MedicalHistory> updateMedicalHistory(@PathVariable("id") long id,
      @RequestBody MedicalHistory MedicalHistory) {
    Optional<MedicalHistory> MedicalHistoryData = medicalHistoryRepository.findById(id);

    if (MedicalHistoryData.isPresent()) {
      return new ResponseEntity<>(medicalHistoryRepository.save(MedicalHistory), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/medical-history/{id}")
  public ResponseEntity<HttpStatus> deleteMedicalHistoryById(@PathVariable("id") long id) {
    try {
      medicalHistoryRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
