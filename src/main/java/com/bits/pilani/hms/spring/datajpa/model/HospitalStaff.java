package com.bits.pilani.hms.spring.datajpa.model;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "hospital_staff")
@Setter
@Getter
@ToString
public class HospitalStaff {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Column(name = "first_name")
  private String first_name;

  @Column(name = "last_name")
  private String last_name;

  @Column(name = "gender")
  private String gender;

  @Column(name = "patient_age")
  private int patient_age;

  @Column(name = "address_line_1")
  private String address_line_1;
  @Column(name = "address_line_2")
  private String address_line_2;
  @Column(name = "phone_number")
  private String phone_number;

  @Column(nullable = false, updatable = false, name = "created_date")
  @CreationTimestamp
  private LocalDateTime created_date;

  @Column(nullable = false, updatable = false, name = "last_updated_date")
  @CreationTimestamp
  private LocalDateTime last_updated_date;

  @Column(name = "created_by")
  private String created_by;

  @Column(name = "last_updated_by")
  private String last_updated_by;

  public HospitalStaff() {
  }

}
