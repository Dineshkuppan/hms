package com.bits.pilani.hms.spring.datajpa.controller;

import com.bits.pilani.hms.spring.datajpa.model.LabTests;
import com.bits.pilani.hms.spring.datajpa.repository.LabTestsRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class LabTestController {

  @Autowired
  LabTestsRepository labTestsRepository;

  @GetMapping("/labtests/all")
  public ResponseEntity<List<LabTests>> getAllLabTests(
      @RequestParam(required = false) String labTestsId) {
    try {
      List<LabTests> labTests = new ArrayList<>();

      if (labTestsId == null) {
        labTests.addAll(labTestsRepository.findAll());
      }

      if (labTests.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(labTests, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/labtests/{id}")
  public ResponseEntity<LabTests> getLabTestsById(@PathVariable("id") long id) {
    Optional<LabTests> recordByData = labTestsRepository.findById(id);
    return recordByData.map(record -> new ResponseEntity<>(record, HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PostMapping("/labtests")
  public ResponseEntity<LabTests> addLabTests(@RequestBody LabTests LabTests) {
    labTestsRepository.save(LabTests);
    try {
      return new ResponseEntity<>(LabTests, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/labtests/{id}")
  public ResponseEntity<LabTests> updateLabTests(@PathVariable("id") long id,
      @RequestBody LabTests LabTests) {
    Optional<LabTests> LabTestsData = labTestsRepository.findById(id);

    if (LabTestsData.isPresent()) {
      return new ResponseEntity<>(labTestsRepository.save(LabTests), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/labtests/{id}")
  public ResponseEntity<HttpStatus> deleteLabTestsById(@PathVariable("id") long id) {
    try {
      labTestsRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
