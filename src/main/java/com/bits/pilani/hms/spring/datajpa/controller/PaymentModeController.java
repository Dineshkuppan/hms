package com.bits.pilani.hms.spring.datajpa.controller;

import com.bits.pilani.hms.spring.datajpa.model.PaymentMode;
import com.bits.pilani.hms.spring.datajpa.repository.PaymentModeRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class PaymentModeController {
  @Autowired
  PaymentModeRepository paymentModeRepository;

  @GetMapping("/paymentmode/all")
  public ResponseEntity<List<PaymentMode>> getAllPaymentModes(
      @RequestParam(required = false) String PaymentModeId) {
    try {
      List<PaymentMode> PaymentModes = new ArrayList<>();

      if (PaymentModeId == null) {
        PaymentModes.addAll(paymentModeRepository.findAll());
      }

      if (PaymentModes.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(PaymentModes, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/paymentmode/{id}")
  public ResponseEntity<PaymentMode> getPaymentModeById(@PathVariable("id") long id) {
    Optional<PaymentMode> recordByData = paymentModeRepository.findById(id);
    return recordByData.map(record -> new ResponseEntity<>(record, HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PostMapping("/paymentmode")
  public ResponseEntity<PaymentMode> addPaymentMode(@RequestBody PaymentMode PaymentMode) {
    paymentModeRepository.save(PaymentMode);
    try {
      return new ResponseEntity<>(PaymentMode, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/paymentmode/{id}")
  public ResponseEntity<PaymentMode> updatePaymentMode(@PathVariable("id") long id,
      @RequestBody PaymentMode PaymentMode) {
    Optional<PaymentMode> PaymentModeData = paymentModeRepository.findById(id);

    if (PaymentModeData.isPresent()) {
      return new ResponseEntity<>(paymentModeRepository.save(PaymentMode), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/paymentmode/{id}")
  public ResponseEntity<HttpStatus> deletePaymentModeById(@PathVariable("id") long id) {
    try {
      paymentModeRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
