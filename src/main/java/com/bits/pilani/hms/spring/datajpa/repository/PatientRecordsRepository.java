package com.bits.pilani.hms.spring.datajpa.repository;

import com.bits.pilani.hms.spring.datajpa.model.PatientRecords;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientRecordsRepository extends JpaRepository<PatientRecords, Long> {

}
