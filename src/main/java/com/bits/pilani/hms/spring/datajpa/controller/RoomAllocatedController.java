package com.bits.pilani.hms.spring.datajpa.controller;

import com.bits.pilani.hms.spring.datajpa.model.RoomAllocated;
import com.bits.pilani.hms.spring.datajpa.repository.RoomAllocatedRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class RoomAllocatedController {

  @Autowired
  RoomAllocatedRepository roomAllocatedRepository;

  @GetMapping("/room-allocated/all")
  public ResponseEntity<List<RoomAllocated>> getAllRoomAllocated(
      @RequestParam(required = false) String roomAllocatedId) {
    try {
      List<RoomAllocated> roomAllocatedList = new ArrayList<>();

      if (roomAllocatedId == null) {
        roomAllocatedList.addAll(roomAllocatedRepository.findAll());
      }

      if (roomAllocatedList.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(roomAllocatedList, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/room-allocated/{id}")
  public ResponseEntity<RoomAllocated> getRoomAllocatedById(@PathVariable("id") long id) {
    Optional<RoomAllocated> recordByData = roomAllocatedRepository.findById(id);
    return recordByData.map(record -> new ResponseEntity<>(record, HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PostMapping("/room-allocated")
  public ResponseEntity<RoomAllocated> addRoomAllocated(@RequestBody RoomAllocated RoomAllocated) {
    roomAllocatedRepository.save(RoomAllocated);
    try {
      return new ResponseEntity<>(RoomAllocated, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/room-allocated/{id}")
  public ResponseEntity<RoomAllocated> updateRoomAllocated(@PathVariable("id") long id,
      @RequestBody RoomAllocated allocated) {
    Optional<RoomAllocated> roomAllocated = roomAllocatedRepository.findById(id);

    if (roomAllocated.isPresent()) {
      return new ResponseEntity<>(roomAllocatedRepository.save(allocated), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/room-allocated/{id}")
  public ResponseEntity<HttpStatus> deleteRoomAllocatedById(@PathVariable("id") long id) {
    try {
      roomAllocatedRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
