package com.bits.pilani.hms.spring.datajpa.controller;

import com.bits.pilani.hms.spring.datajpa.model.Physician;
import com.bits.pilani.hms.spring.datajpa.repository.PhysicianRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class PhysicianController {

  @Autowired
  PhysicianRepository physicianRepository;

  @GetMapping("/physician/all")
  public ResponseEntity<List<Physician>> getAllPhysicians(
      @RequestParam(required = false) String physicianId) {
    try {
      List<Physician> physicianList = new ArrayList<>();

      if (physicianId == null) {
        physicianList.addAll(physicianRepository.findAll());
      }

      if (physicianList.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(physicianList, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/physician/{id}")
  public ResponseEntity<Physician> getPhysicianById(@PathVariable("id") long id) {
    Optional<Physician> recordByData = physicianRepository.findById(id);
    return recordByData.map(record -> new ResponseEntity<>(record, HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PostMapping("/physician")
  public ResponseEntity<Physician> addPhysician(@RequestBody Physician physician) {
    physicianRepository.save(physician);
    try {
      return new ResponseEntity<>(physician, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/physician/{id}")
  public ResponseEntity<Physician> updatePhysician(@PathVariable("id") long id,
      @RequestBody Physician physician) {
    Optional<Physician> physicianData = physicianRepository.findById(id);

    if (physicianData.isPresent()) {
      return new ResponseEntity<>(physicianRepository.save(physician), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/physician/{id}")
  public ResponseEntity<HttpStatus> deletePhysicianById(@PathVariable("id") long id) {
    try {
      physicianRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
