package com.bits.pilani.hms.spring.datajpa.controller;

import com.bits.pilani.hms.spring.datajpa.model.RoomType;
import com.bits.pilani.hms.spring.datajpa.repository.RoomTypeRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class RoomTypeController {

  @Autowired
  RoomTypeRepository roomTypeRepository;

  @GetMapping("/room-type/all")
  public ResponseEntity<List<RoomType>> getAllRoomTypes(
      @RequestParam(required = false) String RoomTypeId) {
    try {
      List<RoomType> roomTypeList = new ArrayList<>();

      if (RoomTypeId == null) {
        roomTypeList.addAll(roomTypeRepository.findAll());
      }

      if (roomTypeList.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(roomTypeList, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/room-type/{id}")
  public ResponseEntity<RoomType> getRoomTypeById(@PathVariable("id") long id) {
    Optional<RoomType> recordByData = roomTypeRepository.findById(id);
    return recordByData.map(record -> new ResponseEntity<>(record, HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PostMapping("/room-type")
  public ResponseEntity<RoomType> addRoomType(@RequestBody RoomType roomType) {
    roomTypeRepository.save(roomType);
    try {
      return new ResponseEntity<>(roomType, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/room-type/{id}")
  public ResponseEntity<RoomType> updateRoomType(@PathVariable("id") long id,
      @RequestBody RoomType roomType) {
    Optional<RoomType> optionalRoomType = roomTypeRepository.findById(id);

    if (optionalRoomType.isPresent()) {
      return new ResponseEntity<>(roomTypeRepository.save(roomType), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/room-type/{id}")
  public ResponseEntity<HttpStatus> deleteRoomTypeById(@PathVariable("id") long id) {
    try {
      roomTypeRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
