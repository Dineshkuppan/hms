package com.bits.pilani.hms.spring.datajpa.controller;

import com.bits.pilani.hms.spring.datajpa.model.Drug;
import com.bits.pilani.hms.spring.datajpa.repository.DrugRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class DrugController {

  @Autowired
  DrugRepository drugRepository;

  @GetMapping("/drug/all")
  public ResponseEntity<List<Drug>> getAllDrugs(
      @RequestParam(required = false) String drugId) {
    try {
      List<Drug> drugList = new ArrayList<>();

      if (drugId == null) {
        drugList.addAll(drugRepository.findAll());
      }

      if (drugList.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(drugList, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/drug/{id}")
  public ResponseEntity<Drug> getDrugById(@PathVariable("id") long id) {
    Optional<Drug> recordByData = drugRepository.findById(id);
    return recordByData.map(record -> new ResponseEntity<>(record, HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PostMapping("/drug")
  public ResponseEntity<Drug> addDrug(@RequestBody Drug Drug) {
    drugRepository.save(Drug);
    try {
      return new ResponseEntity<>(Drug, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/drug/{id}")
  public ResponseEntity<Drug> updateDrug(@PathVariable("id") long id,
      @RequestBody Drug Drug) {
    Optional<Drug> DrugData = drugRepository.findById(id);

    if (DrugData.isPresent()) {
      return new ResponseEntity<>(drugRepository.save(Drug), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/drug/{id}")
  public ResponseEntity<HttpStatus> deleteDrugById(@PathVariable("id") long id) {
    try {
      drugRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
