package com.bits.pilani.hms.spring.datajpa.repository;

import com.bits.pilani.hms.spring.datajpa.model.RoomAllocated;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomAllocatedRepository extends JpaRepository<RoomAllocated, Long> {

}
