package com.bits.pilani.hms.spring.datajpa.controller;

import com.bits.pilani.hms.spring.datajpa.model.ShiftTimings;
import com.bits.pilani.hms.spring.datajpa.repository.ShiftTimingsRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class ShiftController {

  @Autowired
  ShiftTimingsRepository shiftTimingsRepository;

  @GetMapping("/shift/all")
  public ResponseEntity<List<ShiftTimings>> getAllShiftTimings(
      @RequestParam(required = false) String shiftTimingsId) {
    try {
      List<ShiftTimings> shiftTimingsList = new ArrayList<>();

      if (shiftTimingsId == null) {
        shiftTimingsList.addAll(shiftTimingsRepository.findAll());
      }

      if (shiftTimingsList.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(shiftTimingsList, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/shift/{id}")
  public ResponseEntity<ShiftTimings> getShiftTimingsById(@PathVariable("id") long id) {
    Optional<ShiftTimings> recordByData = shiftTimingsRepository.findById(id);
    return recordByData.map(record -> new ResponseEntity<>(record, HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PostMapping("/shift")
  public ResponseEntity<ShiftTimings> addShiftTimings(@RequestBody ShiftTimings ShiftTimings) {
    shiftTimingsRepository.save(ShiftTimings);
    try {
      return new ResponseEntity<>(ShiftTimings, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/shift/{id}")
  public ResponseEntity<ShiftTimings> updateShiftTimings(@PathVariable("id") long id,
      @RequestBody ShiftTimings shiftTimings) {
    Optional<ShiftTimings> optionalShiftTimings = shiftTimingsRepository.findById(id);

    if (optionalShiftTimings.isPresent()) {
      return new ResponseEntity<>(shiftTimingsRepository.save(shiftTimings), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/shift/{id}")
  public ResponseEntity<HttpStatus> deleteShiftTimingsById(@PathVariable("id") long id) {
    try {
      shiftTimingsRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
